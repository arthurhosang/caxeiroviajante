﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.AlgoritmoGenetico
{
    public class Populacao
    {
        #region Atributos
        public int TamanhoGeracao;
        public int QtdeGeracoes;
        public decimal TaxaCruzamento;
        public decimal TaxaMutacao;
        public int qtdeCidades;
        private Mapa Mapa;
        public List<Geracao> Geracoes;
        #endregion

        #region Inicialização
        public Populacao(int tamanhoGeracao, decimal taxaCruzamento, decimal taxaMutacao, Mapa mapa, int qtdeCidades, int qtdeGeracoes)
        {
            this.TaxaCruzamento = taxaCruzamento;
            this.TaxaMutacao = taxaMutacao;
            this.TamanhoGeracao = tamanhoGeracao;
            this.QtdeGeracoes = qtdeGeracoes;
            this.qtdeCidades = qtdeCidades;
            this.Mapa = mapa;
            Geracoes = new List<Geracao>();
        }
        #endregion

        #region Iniciar
        public void RealizarCruzamento()
        {
            //População Inicial
            Geracao geracaoAtual = new Geracao(this.Mapa, "0", this.TamanhoGeracao, this.qtdeCidades, this.TaxaMutacao);
            geracaoAtual.GerarPopulacaoInicial(this.qtdeCidades);
            Geracoes.Add(geracaoAtual);
            

            //Novas Gerações
            for (int i = 1; i < this.QtdeGeracoes; i++)
            {
                geracaoAtual = new Geracao(this.Mapa, i.ToString(), this.TamanhoGeracao, this.qtdeCidades, this.TaxaMutacao);
                geracaoAtual.GerarNovaGeracao(this.Geracoes.Last(), this.TaxaCruzamento);
                this.Geracoes.Add(geracaoAtual);
            }

            System.Diagnostics.Debug.Write("\n\n Melhor Indivíduo: ");
        }
        #endregion      
    }
}
