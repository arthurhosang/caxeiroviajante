﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.AlgoritmoGenetico
{
    public class Geracao
    {
        #region Atributos
        public string Codigo;
        public List<Individuo> Individuos;
        private Mapa Mapa;
        private int TamanhoAlelo;
        private int TamanhoGeracao;
        private decimal TaxaMutacao;
        #endregion

        #region Inicialização
        public Geracao(Mapa mapa, string codigo, int tamanhoGeracao, int tamanhoAlelo, decimal taxaMutacao)
        {
            this.Codigo = codigo;
            this.TamanhoAlelo = tamanhoAlelo;
            this.TamanhoGeracao = tamanhoGeracao;
            this.TaxaMutacao = taxaMutacao;
            this.Individuos = new List<Individuo>();
            this.Mapa = mapa;
        }
        #endregion

        #region Criação de Novas Gerações
        public void GerarPopulacaoInicial(int qtdeCromossomos)
        {
            Random rnd = new Random(new DateTime().Second);

            for (int i = 0; i < this.TamanhoGeracao; i++)
            {
                List<int> cromossomos = Enumerable.Range(0, this.TamanhoAlelo).OrderBy(x => rnd.Next()).Take(qtdeCromossomos).ToList();

                Individuo individuo = new Individuo(cromossomos, this.Mapa, i.ToString() , this.TamanhoAlelo, new List<int>(), new List<int>(),0,0);
                this.Individuos.Add(individuo);
            }
        }

        public void GerarNovaGeracao(Geracao geracaoAnterior, decimal taxaCruzamento)
        {
            int populacao = geracaoAnterior.Individuos.Count;

            //Calcula a quantidade de Cruzamentos que serão realizados
            decimal aux = geracaoAnterior.Individuos.Count * taxaCruzamento / 2;
            aux = Math.Round(aux);
            int qtdeCruzamentos = int.Parse(aux.ToString());

            List<Individuo> individuosSolteiros = new List<Individuo>();
            individuosSolteiros.AddRange(geracaoAnterior.Individuos);
            List<Individuo> individuosNascidos = new List<Individuo>();

            Random rndCruzamento = new Random();
            Random rndMutacao = new Random();

            for (int i = 0; i < qtdeCruzamentos; i++)
            {
                Individuo individuoA;
                Individuo individuoB;

                #region Selecionar
                individuoA = individuosSolteiros[this.Selecionar(individuosSolteiros)];
                individuosSolteiros.Remove(individuoA);
                individuoB = individuosSolteiros[this.Selecionar(individuosSolteiros)];
                individuosSolteiros.Remove(individuoB);
                #endregion           

                #region Cruzar
                //Realiza o cruzamento do Indivíduo A com o Indivíduo B
                individuosNascidos.AddRange(Cruzar(individuoA, individuoB, i*2, rndCruzamento.Next(1, individuoA.Cromossomos.Count - 1)));
                #endregion
            }


            #region Mutar
            for (int i = 0; i < ((int)Math.Floor(individuosNascidos.Count * this.TaxaMutacao)); i++)
            {
                int individuoAMutar = rndMutacao.Next(0, individuosNascidos.Count - 1);
                individuosNascidos[individuoAMutar].SofrerMutacao(this.Mapa);
            }
            #endregion

            /*foreach (Individuo item in individuosSolteiros)
            {
                item.QtdeGenesA = -2;
            }*/

            /*
            for(int x=0;x< individuosSolteiros.Count; x++)
            {
                individuosSolteiros[x].QtdeGenesA = -2;
                individuosSolteiros[x].QtdeMutacoes = -1;
                individuosSolteiros[x].CromossomosPaiA = new List<int>();
                individuosSolteiros[x].CromossomosPaiB = new List<int>();                
            }
            */

            this.Individuos = new List<Individuo>();
            this.Individuos.AddRange(individuosNascidos);
            this.Individuos.AddRange(individuosSolteiros);
        }
        #endregion

        #region Seleção
        public int Selecionar(List<Individuo> individuosSolteiros)
        {
            int posMelhor = 0;
            int fitnessMelhor = 0;

                    
            Random rnd = new Random(individuosSolteiros.Count);
            return rnd.Next(0, individuosSolteiros.Count);

            /* 
            for (int i = 0; i < individuosSolteiros.Count; i++)
            {
                if (individuosSolteiros[i].Fitness < fitnessMelhor || fitnessMelhor == 0)
                {
                    posMelhor = i;
                    fitnessMelhor = individuosSolteiros[i].Fitness;
                }
            }
            */


            return posMelhor;
        }
        #endregion

        #region Cruzamento
        public List<Individuo> Cruzar(Individuo individuoA, Individuo individuoB, int codigoIndividuo, int pontoCorte)
        {
            codigoIndividuo++;
            List<Individuo> retorno = new List<Individuo>();
            int qtdeCromossomos = individuoA.Cromossomos.Count;

            //O Ponto de Corte é definido Aleatoriamente
            int qtdeCromossomosAlfa = pontoCorte;
            int qtdeCromossomosBeta = individuoA.Cromossomos.Count - pontoCorte;
            
            retorno.Add(new Individuo(GeraCromossomo(individuoA, individuoB, qtdeCromossomosAlfa, qtdeCromossomosBeta), this.Mapa, codigoIndividuo.ToString(), this.TamanhoAlelo, individuoA.Cromossomos, individuoB.Cromossomos, this.TaxaMutacao, qtdeCromossomosAlfa));


            codigoIndividuo++;
            retorno.Add(new Individuo(GeraCromossomo(individuoB, individuoA, qtdeCromossomosAlfa, qtdeCromossomosBeta), this.Mapa,  codigoIndividuo.ToString(), this.TamanhoAlelo, individuoB.Cromossomos, individuoA.Cromossomos, this.TaxaMutacao, qtdeCromossomosAlfa));

            return retorno;
        }

        public List<int> GeraCromossomo(Individuo individuoA, Individuo individuoB, int qtdeCromossomosAlfa, int qtdeCromossomosBeta)
        {
            List<int> cromossomoFilho = new List<int>();
            Random rnd = new Random();

            for (int i = 0; i < qtdeCromossomosAlfa; i++)
            {
                cromossomoFilho.Add(individuoA.Cromossomos[i]);
            }

            for (int i = qtdeCromossomosAlfa; i < qtdeCromossomosAlfa + qtdeCromossomosBeta; i++)
            {
                cromossomoFilho.Add(individuoB.Cromossomos[i]);
            }

            return cromossomoFilho;
        }
        #endregion
    }
}
