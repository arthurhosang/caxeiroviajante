﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.AlgoritmoGenetico
{
    public class Individuo
    {
        #region Atributos
        public string Codigo;
        public int Fitness;
        public int TamanhoAlelo;
        public int QtdeMutacoes = 0;
        private int qtdeGenesA;

        public List<int> Cromossomos;
        public List<int> CromossomosPaiA;
        public List<int> CromossomosPaiB;

        public int QtdeGenesA
        {
            get
            {
                return qtdeGenesA;
            }

            set
            {
                qtdeGenesA = value;
            }
        }
        #endregion

        #region Inicialização
        public Individuo(List<int> cromossomos, Mapa mapa, string codigo, int tamanhoAlelo, List<int> cromossomosPaiA, List<int> cromossomosPaiB, decimal taxaMutacao, int qtdeGenesA)
        {
            this.Codigo = codigo;
            this.Cromossomos = cromossomos;
            this.TamanhoAlelo = tamanhoAlelo;

            this.CromossomosPaiA = cromossomosPaiA;
            this.CromossomosPaiB = cromossomosPaiB;

            this.QtdeGenesA = qtdeGenesA;

            VerificaDuplicidade();
            this.CalcularFitness(mapa);

            #region Mutação
            /*
            string agora = DateTime.Now.Ticks.ToString(); 
            Random rnd = new Random(int.Parse(agora.Substring(agora.Length - 4, 3))* int.Parse(codigo));
            int random = rnd.Next(0, 1);
            if (random < taxaMutacao)
                this.SofrerMutacao(mapa);
            */
            #endregion

            System.Diagnostics.Debug.Write("\n Novo ");
            ImprimeInformacoes();
        }

        public Individuo()
        {
            this.Codigo = "-2";
            this.Cromossomos = new List<int>();
            this.TamanhoAlelo = 0;
            this.QtdeGenesA = -1;

            this.CromossomosPaiA = new List<int>();
            this.CromossomosPaiB = new List<int>();

            this.Fitness = 0;
        }
        #endregion

        #region Fitness
        public void CalcularFitness(Mapa mapa)
        {
            this.Fitness = 0;
            List<int> cromossomos = new List<int>();
            cromossomos.AddRange(this.Cromossomos);
            cromossomos.Add(this.Cromossomos.First());

            for (int i = 1; i < cromossomos.Count; i++)
            {
                this.Fitness += mapa.CalcularDistancia(cromossomos[i - 1], cromossomos[i]);
            }
        }
        #endregion

        #region Mutação
        public void SofrerMutacao(Mapa mapa)
        {
            this.QtdeMutacoes++;
            Random rnd = new Random();
            int geneA = rnd.Next(0, this.Cromossomos.Count - 1);
            int geneB;

            do
            {
                geneB = rnd.Next(0, this.Cromossomos.Count - 1);
            } while (geneA == geneB);

            int aux = this.Cromossomos[geneA];
            this.Cromossomos[geneA] = this.Cromossomos[geneB];
            this.Cromossomos[geneB] = aux;           

            this.VerificaDuplicidade();
            this.CalcularFitness(mapa);
        }
        #endregion

        #region Validação
        public void VerificaDuplicidade()
        {
            #region Verifica Genes Duplicados
            List<int> genesNoCromossomo = new List<int>();
            List<int> alelos = Enumerable.Range(0, this.TamanhoAlelo).ToList();

            //Altera os genes Duplicados do Cromossomo Gerado para -1
            for (int i = 0; i < this.Cromossomos.Count; i++)
            {
                if (genesNoCromossomo.Contains(this.Cromossomos[i]))
                {
                    this.Cromossomos[i] = -1;
                }
                else
                {
                    alelos.Remove(this.Cromossomos[i]);
                    genesNoCromossomo.Add(this.Cromossomos[i]);
                }
            }

            //Altera os genes iguais a -1 para valores que não estão no cromossomo
            for (int i = 0; i < this.Cromossomos.Count; i++)
            {
                if (this.Cromossomos[i] == -1)
                {
                    this.Cromossomos[i] = alelos[0];
                    alelos.RemoveAt(0);
                }
            }
            #endregion
        }
        #endregion

        #region Debug
        public void ImprimeInformacoes()
        {
            System.Diagnostics.Debug.Write("Indivíduo " + this.Codigo + " ==> ");
            foreach (int item in this.Cromossomos)
            {
                System.Diagnostics.Debug.Write(item.ToString() + ", ");
            }
            System.Diagnostics.Debug.Write("; Fitness: " + this.Fitness);
        }
        #endregion
    }
}
