﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Distancias : Form
    {
        private int QtdeCidades;

        public Distancias(int qtdeCidades)
        {
            InitializeComponent();
            this.QtdeCidades = qtdeCidades;

            this.lblQtdeCidades.Text = "Seu mapa possui " + this.QtdeCidades + " cidades. Informe abaixo a distância entre as cidades:";
            this.InicializaTabela();

            this.atualizaPrgQtdePreenchida(0, 0);
        }

        private void InicializaTabela()
        {
            object[] linha = new object[3];

            for (int i = 0; i < this.QtdeCidades - 1; i++)
            {
                for (int j = i + 1; j < this.QtdeCidades; j++)
                {
                    linha[0] = new Alfabeto(this.QtdeCidades).GetLetraByPosicao(i);
                    linha[1] = new Alfabeto(this.QtdeCidades).GetLetraByPosicao(j);
                    linha[2] = "";
                    grdDistancias.Rows.Add(linha);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (validacao())
            {
                decimal taxaCruzamento = (decimal)edtTaxaCruzamento.Value / 100;
                decimal taxaMutacao = (decimal)edtTaxaMutacao.Value / 100;
                int tamanhoGeracao = (int)edtTamGeracao.Value;
                int qtdeGeracoes = (int)edtQtdeGeracoes.Value;


                Mapa mapa = new Mapa(this.grdDistancias.Rows.Count);
                int cidadeA;
                int cidadeB;
                int distancia;

                foreach (DataGridViewRow item in grdDistancias.Rows)
                {
                    cidadeA = new Alfabeto(this.QtdeCidades).GetPosicaoByLetra(item.Cells[0].Value.ToString());
                    cidadeB = new Alfabeto(this.QtdeCidades).GetPosicaoByLetra(item.Cells[1].Value.ToString());
                    distancia = int.Parse(item.Cells[2].Value.ToString());

                    mapa.Add(cidadeA, cidadeB, distancia);
                }
                Resultado resultado = new Resultado(chcUltimoGene.Checked);
                resultado.pop = new AlgoritmoGenetico.Populacao(tamanhoGeracao, taxaCruzamento, taxaMutacao, mapa, this.QtdeCidades, qtdeGeracoes);
                resultado.Calcular();
                resultado.Visible = true;
            }
        }

        private void grdDistancias_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int preenchido = 0;

            foreach (DataGridViewRow item in grdDistancias.Rows)
            {
                if (item.Cells[2].Value.ToString() != "")
                {
                    preenchido++;
                }
            }
            this.atualizaPrgQtdePreenchida(preenchido, grdDistancias.Rows.Count);
        }

        private void atualizaPrgQtdePreenchida(int qtdePrenchida, int qtdeTotal)
        {
            prgQtdePreenchida.Maximum = qtdeTotal;
            prgQtdePreenchida.Value = qtdePrenchida;

            ToolTip TP = new ToolTip();
            TP.ShowAlways = true;
            TP.SetToolTip(prgQtdePreenchida, qtdePrenchida + "/" + qtdeTotal + " distância(s) preenchida(s)");
        }

        private bool validacao()
        {
            int aux;
            foreach (DataGridViewRow item in grdDistancias.Rows)
            {
                if (String.IsNullOrEmpty(item.Cells[2].Value.ToString()))
                {
                    MessageBox.Show("Por favor informe todas as distâncias.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    grdDistancias.CurrentCell = item.Cells[2];
                    return false;
                }
                else
                {
                    if (!int.TryParse(item.Cells[2].Value.ToString(), out aux))
                    {
                        MessageBox.Show("Por favor informe uma distância válida.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        grdDistancias.CurrentCell = item.Cells[2] ;
                        return false;
                    }

                }
            }
            return true;
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            grdDistancias[2, 0].Value = "2";
            grdDistancias[2, 1].Value = "5";
            grdDistancias[2, 2].Value = "4";
            grdDistancias[2, 3].Value = "36";
            grdDistancias[2, 4].Value = "15";
            grdDistancias[2, 5].Value = "13";
            grdDistancias[2, 6].Value = "40";
            grdDistancias[2, 7].Value = "25";
            grdDistancias[2, 8].Value = "10";
            grdDistancias[2, 9].Value = "37";
        }

        private void preencherCom4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            grdDistancias[2, 0].Value = "2";
            grdDistancias[2, 1].Value = "5";
            grdDistancias[2, 2].Value = "4";
            grdDistancias[2, 3].Value = "36";
            grdDistancias[2, 4].Value = "15";
            grdDistancias[2, 5].Value = "13";
        }

        private void aleatóriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Random rnd;
            for (int i =0; i<grdDistancias.RowCount; i++)
            {
                rnd = new Random(i);
                grdDistancias[2, i].Value = rnd.Next(0, 100);
            }
        }
    }
}
