﻿namespace WindowsFormsApplication1
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.edtQtdeCidades = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.btIniciar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.edtQtdeCidades)).BeginInit();
            this.SuspendLayout();
            // 
            // edtQtdeCidades
            // 
            this.edtQtdeCidades.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.edtQtdeCidades.Location = new System.Drawing.Point(219, 7);
            this.edtQtdeCidades.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.edtQtdeCidades.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.edtQtdeCidades.Name = "edtQtdeCidades";
            this.edtQtdeCidades.Size = new System.Drawing.Size(120, 20);
            this.edtQtdeCidades.TabIndex = 0;
            this.edtQtdeCidades.ThousandsSeparator = true;
            this.edtQtdeCidades.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.edtQtdeCidades.KeyDown += new System.Windows.Forms.KeyEventHandler(this.edtQtdeCidades_KeyDown);
            this.edtQtdeCidades.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.edtQtdeCidades_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Quantidade de Cidades";
            // 
            // btIniciar
            // 
            this.btIniciar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btIniciar.Location = new System.Drawing.Point(283, 226);
            this.btIniciar.Name = "btIniciar";
            this.btIniciar.Size = new System.Drawing.Size(75, 23);
            this.btIniciar.TabIndex = 2;
            this.btIniciar.Text = "Iniciar";
            this.btIniciar.UseVisualStyleBackColor = true;
            this.btIniciar.Click += new System.EventHandler(this.btIniciar_Click);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 261);
            this.Controls.Add(this.btIniciar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.edtQtdeCidades);
            this.Name = "Principal";
            this.Text = "Caxeiro Viajante";
            this.Load += new System.EventHandler(this.Principal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.edtQtdeCidades)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown edtQtdeCidades;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btIniciar;
    }
}

