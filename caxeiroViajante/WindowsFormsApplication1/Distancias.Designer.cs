﻿namespace WindowsFormsApplication1
{
    partial class Distancias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblQtdeCidades = new System.Windows.Forms.Label();
            this.prgQtdePreenchida = new System.Windows.Forms.ProgressBar();
            this.button1 = new System.Windows.Forms.Button();
            this.grdDistancias = new System.Windows.Forms.DataGridView();
            this.origem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.destino = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.distancia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.preencherCom4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.aleatóriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.edtQtdeGeracoes = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.edtTamGeracao = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.edtTaxaMutacao = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.edtTaxaCruzamento = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.chcUltimoGene = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.grdDistancias)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtQtdeGeracoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtTamGeracao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtTaxaMutacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtTaxaCruzamento)).BeginInit();
            this.SuspendLayout();
            // 
            // lblQtdeCidades
            // 
            this.lblQtdeCidades.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQtdeCidades.AutoSize = true;
            this.lblQtdeCidades.Location = new System.Drawing.Point(12, 9);
            this.lblQtdeCidades.Name = "lblQtdeCidades";
            this.lblQtdeCidades.Size = new System.Drawing.Size(354, 13);
            this.lblQtdeCidades.TabIndex = 0;
            this.lblQtdeCidades.Text = "Seu mapa possui X  cidades. Informe abaixo a distância entre as cidades:";
            // 
            // prgQtdePreenchida
            // 
            this.prgQtdePreenchida.AccessibleDescription = "";
            this.prgQtdePreenchida.AccessibleName = "";
            this.prgQtdePreenchida.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.prgQtdePreenchida.Location = new System.Drawing.Point(12, 338);
            this.prgQtdePreenchida.Name = "prgQtdePreenchida";
            this.prgQtdePreenchida.Size = new System.Drawing.Size(693, 23);
            this.prgQtdePreenchida.Step = 1;
            this.prgQtdePreenchida.TabIndex = 1;
            this.prgQtdePreenchida.Tag = "";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(711, 338);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Iniciar Análise";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // grdDistancias
            // 
            this.grdDistancias.AllowUserToAddRows = false;
            this.grdDistancias.AllowUserToDeleteRows = false;
            this.grdDistancias.AllowUserToOrderColumns = true;
            this.grdDistancias.AllowUserToResizeColumns = false;
            this.grdDistancias.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdDistancias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDistancias.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.origem,
            this.destino,
            this.distancia});
            this.grdDistancias.ContextMenuStrip = this.contextMenuStrip1;
            this.grdDistancias.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdDistancias.Location = new System.Drawing.Point(12, 25);
            this.grdDistancias.Name = "grdDistancias";
            this.grdDistancias.Size = new System.Drawing.Size(450, 307);
            this.grdDistancias.TabIndex = 3;
            this.grdDistancias.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdDistancias_CellValueChanged);
            // 
            // origem
            // 
            this.origem.HeaderText = "Cidade Origem";
            this.origem.Name = "origem";
            this.origem.ReadOnly = true;
            // 
            // destino
            // 
            this.destino.HeaderText = "Cidade Destino";
            this.destino.Name = "destino";
            this.destino.ReadOnly = true;
            // 
            // distancia
            // 
            this.distancia.HeaderText = "Distância";
            this.distancia.Name = "distancia";
            this.distancia.Width = 150;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.preencherCom4ToolStripMenuItem,
            this.toolStripMenuItem2,
            this.toolStripMenuItem1,
            this.aleatóriosToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(263, 76);
            this.contextMenuStrip1.Text = "Preencher para 5 cidades";
            // 
            // preencherCom4ToolStripMenuItem
            // 
            this.preencherCom4ToolStripMenuItem.Name = "preencherCom4ToolStripMenuItem";
            this.preencherCom4ToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.preencherCom4ToolStripMenuItem.Text = "Preencher com 4";
            this.preencherCom4ToolStripMenuItem.Click += new System.EventHandler(this.preencherCom4ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(262, 22);
            this.toolStripMenuItem2.Text = "Preencher com 5";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(259, 6);
            // 
            // aleatóriosToolStripMenuItem
            // 
            this.aleatóriosToolStripMenuItem.Name = "aleatóriosToolStripMenuItem";
            this.aleatóriosToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.aleatóriosToolStripMenuItem.Text = "Preencher com Números Aleatórios";
            this.aleatóriosToolStripMenuItem.Click += new System.EventHandler(this.aleatóriosToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.chcUltimoGene);
            this.panel1.Controls.Add(this.edtQtdeGeracoes);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.edtTamGeracao);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.edtTaxaMutacao);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.edtTaxaCruzamento);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(468, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(343, 320);
            this.panel1.TabIndex = 4;
            // 
            // edtQtdeGeracoes
            // 
            this.edtQtdeGeracoes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.edtQtdeGeracoes.Location = new System.Drawing.Point(197, 37);
            this.edtQtdeGeracoes.Name = "edtQtdeGeracoes";
            this.edtQtdeGeracoes.Size = new System.Drawing.Size(120, 20);
            this.edtQtdeGeracoes.TabIndex = 22;
            this.edtQtdeGeracoes.Value = new decimal(new int[] {
            6,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Quantidade Gerações";
            // 
            // edtTamGeracao
            // 
            this.edtTamGeracao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.edtTamGeracao.Location = new System.Drawing.Point(197, 11);
            this.edtTamGeracao.Name = "edtTamGeracao";
            this.edtTamGeracao.Size = new System.Drawing.Size(120, 20);
            this.edtTamGeracao.TabIndex = 20;
            this.edtTamGeracao.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Tamanho da Geração";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(321, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "%";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(321, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "%";
            // 
            // edtTaxaMutacao
            // 
            this.edtTaxaMutacao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.edtTaxaMutacao.Location = new System.Drawing.Point(197, 115);
            this.edtTaxaMutacao.Name = "edtTaxaMutacao";
            this.edtTaxaMutacao.Size = new System.Drawing.Size(120, 20);
            this.edtTaxaMutacao.TabIndex = 16;
            this.edtTaxaMutacao.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(192, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Taxa Mutação sobre Novos Indivíduos";
            // 
            // edtTaxaCruzamento
            // 
            this.edtTaxaCruzamento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.edtTaxaCruzamento.Location = new System.Drawing.Point(197, 89);
            this.edtTaxaCruzamento.Name = "edtTaxaCruzamento";
            this.edtTaxaCruzamento.Size = new System.Drawing.Size(120, 20);
            this.edtTaxaCruzamento.TabIndex = 14;
            this.edtTaxaCruzamento.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Taxa Cruzamento";
            // 
            // chcUltimoGene
            // 
            this.chcUltimoGene.AutoSize = true;
            this.chcUltimoGene.Location = new System.Drawing.Point(6, 175);
            this.chcUltimoGene.Name = "chcUltimoGene";
            this.chcUltimoGene.Size = new System.Drawing.Size(110, 17);
            this.chcUltimoGene.TabIndex = 23;
            this.chcUltimoGene.Text = "Exibir último Gene";
            this.chcUltimoGene.UseVisualStyleBackColor = true;
            // 
            // Distancias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(823, 373);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.grdDistancias);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.prgQtdePreenchida);
            this.Controls.Add(this.lblQtdeCidades);
            this.MinimumSize = new System.Drawing.Size(812, 241);
            this.Name = "Distancias";
            this.Text = "Cadastro de Distância entre Cidades";
            ((System.ComponentModel.ISupportInitialize)(this.grdDistancias)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtQtdeGeracoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtTamGeracao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtTaxaMutacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtTaxaCruzamento)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblQtdeCidades;
        private System.Windows.Forms.ProgressBar prgQtdePreenchida;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView grdDistancias;
        private System.Windows.Forms.DataGridViewTextBoxColumn origem;
        private System.Windows.Forms.DataGridViewTextBoxColumn destino;
        private System.Windows.Forms.DataGridViewTextBoxColumn distancia;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem preencherCom4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown edtQtdeGeracoes;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown edtTamGeracao;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown edtTaxaMutacao;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown edtTaxaCruzamento;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aleatóriosToolStripMenuItem;
        private System.Windows.Forms.CheckBox chcUltimoGene;
    }
}