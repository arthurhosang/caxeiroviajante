﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1 {
    public class Mapa {
        #region Atributos
        public int[,] MapaCidades;
        public int LastIndex;
        #endregion

        #region Inicialização
        public Mapa(int tamanho) {
            this.LastIndex = -1;
            MapaCidades = new int[tamanho, 3];
        }
        #endregion

        public void Add(int cidadeA, int cidadeB, int distancia) {
            this.LastIndex++;
            MapaCidades[this.LastIndex, 0] = cidadeA;
            MapaCidades[this.LastIndex, 1] = cidadeB;
            MapaCidades[this.LastIndex, 2] = distancia;
        }

        public int CalcularDistancia(int cidadeA, int cidadeB) {
            if (cidadeA == cidadeB) {
                throw new Exception("A Cidade A não pode ser igual a Cidade B");
            }

            if (cidadeA > cidadeB) {
                int aux = cidadeA;
                cidadeA = cidadeB;
                cidadeB = aux;
            }

            for (int i = 0; i <= LastIndex; i++) {
                if (MapaCidades[i, 0] == cidadeA) {
                    if (MapaCidades[i, 1] == cidadeB) {
                        return MapaCidades[i, 2];
                    }
                }
            }

            throw new Exception("Cidade Não encontrada!");
        }
    }
}
