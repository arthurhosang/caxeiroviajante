﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1 {
    public partial class Principal : Form {
        public Principal() {
            InitializeComponent();
        }

        private void btIniciar_Click(object sender, EventArgs e) {                   
            Distancias distancia = new Distancias((int)edtQtdeCidades.Value);
            distancia.Visible = true;
        }

        private void Principal_Load(object sender, EventArgs e)
        {

        }

        private void edtQtdeCidades_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void edtQtdeCidades_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btIniciar_Click(null, null);
            }
        }
    }
}
