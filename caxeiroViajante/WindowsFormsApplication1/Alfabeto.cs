﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Alfabeto
    {
        private string[] letras;

        public Alfabeto(int alelo)
        {
            if (alelo >= 26)
            {
                letras = null;
            }else { 
                letras = new string[26];

                #region Iniciando Vetor de Letras          
                int i = 0;
                letras[i] = "A";
                i++;
                letras[i] = "B";
                i++;
                letras[i] = "C";
                i++;
                letras[i] = "D";
                i++;
                letras[i] = "E";
                i++;
                letras[i] = "F";
                i++;
                letras[i] = "G";
                i++;
                letras[i] = "H";
                i++;
                letras[i] = "I";
                i++;
                letras[i] = "J";
                i++;
                letras[i] = "K";
                i++;
                letras[i] = "L";
                i++;
                letras[i] = "M";
                i++;
                letras[i] = "N";
                i++;
                letras[i] = "O";
                i++;
                letras[i] = "P";
                i++;
                letras[i] = "Q";
                i++;
                letras[i] = "R";
                i++;
                letras[i] = "S";
                i++;
                letras[i] = "T";
                i++;
                letras[i] = "U";
                i++;
                letras[i] = "V";
                i++;
                letras[i] = "W";
                i++;
                letras[i] = "X";
                i++;
                letras[i] = "Y";
                i++;
                letras[i] = "Z";
                i++;
            #endregion
            }
        }

        public string GetLetraByPosicao(int pos)
        {
            if (letras != null)
            {
                return letras[pos];
            }
            else
            {
                return pos.ToString();
            }
        }

        public int GetPosicaoByLetra(string letra)
        {
            if (letras != null)
            {
                for (int i = 0; i < letras.Length; i++)
                {
                    if (letra == letras[i])
                    {
                        return i;
                    }
                }

                return -1;
            }
            else
            {
                return int.Parse(letra);
            }
        }
    }
}
