﻿namespace WindowsFormsApplication1
{
    partial class Resultado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdResultado = new System.Windows.Forms.DataGridView();
            this.geracao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.individuo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mutacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtdeGenesPaiA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cromossomosA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cromossomosB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rota = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fittness = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTamanhoGeracao = new System.Windows.Forms.Label();
            this.lblMelhor = new System.Windows.Forms.Label();
            this.lblTamanhoAlelo = new System.Windows.Forms.Label();
            this.lblTamanhoPopulacao = new System.Windows.Forms.Label();
            this.lblTaxaPopulacao = new System.Windows.Forms.Label();
            this.lblTaxaCruzamento = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdResultado)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grdResultado
            // 
            this.grdResultado.AllowUserToOrderColumns = true;
            this.grdResultado.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdResultado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdResultado.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.geracao,
            this.individuo,
            this.mutacao,
            this.qtdeGenesPaiA,
            this.cromossomosA,
            this.cromossomosB,
            this.rota,
            this.fittness});
            this.grdResultado.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grdResultado.Location = new System.Drawing.Point(-1, 125);
            this.grdResultado.Name = "grdResultado";
            this.grdResultado.Size = new System.Drawing.Size(853, 185);
            this.grdResultado.TabIndex = 1;
            // 
            // geracao
            // 
            this.geracao.HeaderText = "Geração";
            this.geracao.Name = "geracao";
            this.geracao.ReadOnly = true;
            this.geracao.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // individuo
            // 
            this.individuo.HeaderText = "Indivíduo";
            this.individuo.Name = "individuo";
            this.individuo.ReadOnly = true;
            this.individuo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // mutacao
            // 
            this.mutacao.HeaderText = "Mutação";
            this.mutacao.Name = "mutacao";
            this.mutacao.ReadOnly = true;
            this.mutacao.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // qtdeGenesPaiA
            // 
            this.qtdeGenesPaiA.HeaderText = "Qtde Genes de A";
            this.qtdeGenesPaiA.Name = "qtdeGenesPaiA";
            this.qtdeGenesPaiA.ReadOnly = true;
            this.qtdeGenesPaiA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cromossomosA
            // 
            this.cromossomosA.HeaderText = "Pai A";
            this.cromossomosA.Name = "cromossomosA";
            this.cromossomosA.ReadOnly = true;
            this.cromossomosA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cromossomosB
            // 
            this.cromossomosB.HeaderText = "Pai B";
            this.cromossomosB.Name = "cromossomosB";
            this.cromossomosB.ReadOnly = true;
            this.cromossomosB.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // rota
            // 
            this.rota.HeaderText = "Rota";
            this.rota.Name = "rota";
            this.rota.ReadOnly = true;
            this.rota.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // fittness
            // 
            this.fittness.HeaderText = "Fitness";
            this.fittness.Name = "fittness";
            this.fittness.ReadOnly = true;
            this.fittness.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.lblTamanhoGeracao);
            this.panel1.Controls.Add(this.lblMelhor);
            this.panel1.Controls.Add(this.lblTamanhoAlelo);
            this.panel1.Controls.Add(this.lblTamanhoPopulacao);
            this.panel1.Controls.Add(this.lblTaxaPopulacao);
            this.panel1.Controls.Add(this.lblTaxaCruzamento);
            this.panel1.Location = new System.Drawing.Point(-1, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(853, 107);
            this.panel1.TabIndex = 2;
            // 
            // lblTamanhoGeracao
            // 
            this.lblTamanhoGeracao.AutoSize = true;
            this.lblTamanhoGeracao.Location = new System.Drawing.Point(13, 39);
            this.lblTamanhoGeracao.Name = "lblTamanhoGeracao";
            this.lblTamanhoGeracao.Size = new System.Drawing.Size(35, 13);
            this.lblTamanhoGeracao.TabIndex = 5;
            this.lblTamanhoGeracao.Text = "label6";
            // 
            // lblMelhor
            // 
            this.lblMelhor.AutoSize = true;
            this.lblMelhor.Location = new System.Drawing.Point(13, 94);
            this.lblMelhor.Name = "lblMelhor";
            this.lblMelhor.Size = new System.Drawing.Size(35, 13);
            this.lblMelhor.TabIndex = 4;
            this.lblMelhor.Text = "label5";
            // 
            // lblTamanhoAlelo
            // 
            this.lblTamanhoAlelo.AutoSize = true;
            this.lblTamanhoAlelo.Location = new System.Drawing.Point(13, 52);
            this.lblTamanhoAlelo.Name = "lblTamanhoAlelo";
            this.lblTamanhoAlelo.Size = new System.Drawing.Size(35, 13);
            this.lblTamanhoAlelo.TabIndex = 3;
            this.lblTamanhoAlelo.Text = "label4";
            // 
            // lblTamanhoPopulacao
            // 
            this.lblTamanhoPopulacao.AutoSize = true;
            this.lblTamanhoPopulacao.Location = new System.Drawing.Point(13, 26);
            this.lblTamanhoPopulacao.Name = "lblTamanhoPopulacao";
            this.lblTamanhoPopulacao.Size = new System.Drawing.Size(35, 13);
            this.lblTamanhoPopulacao.TabIndex = 2;
            this.lblTamanhoPopulacao.Text = "label3";
            // 
            // lblTaxaPopulacao
            // 
            this.lblTaxaPopulacao.AutoSize = true;
            this.lblTaxaPopulacao.Location = new System.Drawing.Point(13, 13);
            this.lblTaxaPopulacao.Name = "lblTaxaPopulacao";
            this.lblTaxaPopulacao.Size = new System.Drawing.Size(35, 13);
            this.lblTaxaPopulacao.TabIndex = 1;
            this.lblTaxaPopulacao.Text = "label2";
            // 
            // lblTaxaCruzamento
            // 
            this.lblTaxaCruzamento.AutoSize = true;
            this.lblTaxaCruzamento.Location = new System.Drawing.Point(13, 0);
            this.lblTaxaCruzamento.Name = "lblTaxaCruzamento";
            this.lblTaxaCruzamento.Size = new System.Drawing.Size(91, 13);
            this.lblTaxaCruzamento.TabIndex = 0;
            this.lblTaxaCruzamento.Text = "Taxa de Mutação";
            // 
            // Resultado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 322);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.grdResultado);
            this.Name = "Resultado";
            this.Text = "Resultado do Algoritmo Genético";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.grdResultado)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grdResultado;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTaxaCruzamento;
        private System.Windows.Forms.Label lblTamanhoAlelo;
        private System.Windows.Forms.Label lblTamanhoPopulacao;
        private System.Windows.Forms.Label lblTaxaPopulacao;
        private System.Windows.Forms.Label lblMelhor;
        private System.Windows.Forms.Label lblTamanhoGeracao;
        private System.Windows.Forms.DataGridViewTextBoxColumn geracao;
        private System.Windows.Forms.DataGridViewTextBoxColumn individuo;
        private System.Windows.Forms.DataGridViewTextBoxColumn mutacao;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtdeGenesPaiA;
        private System.Windows.Forms.DataGridViewTextBoxColumn cromossomosA;
        private System.Windows.Forms.DataGridViewTextBoxColumn cromossomosB;
        private System.Windows.Forms.DataGridViewTextBoxColumn rota;
        private System.Windows.Forms.DataGridViewTextBoxColumn fittness;
    }
}