﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.AlgoritmoGenetico;

namespace WindowsFormsApplication1
{
    public partial class Resultado : Form
    {
        public Populacao pop;
        public bool UltimoGene;

        public Resultado(bool ultimoGene)
        {
            this.UltimoGene = ultimoGene;
            InitializeComponent();
        }

        public void Calcular()
        {
            pop.RealizarCruzamento();
            int i = 0;
            int tamanhoPopulacaoReal = 0;

            foreach (Geracao geracao in pop.Geracoes)
            {
                object[] linha = new object[8];
                i++;
                linha[0] = "Geração " + i;
                linha[1] = "";
                linha[2] = "";
                linha[3] = "";
                linha[4] = "";
                linha[5] = "";
                linha[6] = "";
                linha[6] = "";
                grdResultado.Rows.Add(linha);

                foreach (Individuo individuo in geracao.Individuos)
                {
                    linha = new object[8];

                    linha[0] = geracao.Codigo;
                    linha[1] = individuo.Codigo;
                    linha[2] = (individuo.QtdeMutacoes > 0 ? "Mutou " + individuo.QtdeMutacoes + " vezes" : "");
                    linha[3] = individuo.QtdeGenesA;
                    linha[4] = GetCromossomosParaVisualizacao(individuo.CromossomosPaiA);
                    linha[5] = GetCromossomosParaVisualizacao(individuo.CromossomosPaiB);
                    linha[6] = GetCromossomosParaVisualizacao(individuo.Cromossomos);
                    linha[7] = individuo.Fitness;

                    grdResultado.Rows.Add(linha);
                    tamanhoPopulacaoReal++;
                }
            }

            //Parâmetros Informados Inicialmente
            lblTaxaCruzamento.Text = "Taxa de Cruzamento: " + pop.TaxaCruzamento * 100 + "%";
            lblTaxaPopulacao.Text = "Taxa de Mutação: " + pop.TaxaMutacao * 100 + "%";
            lblTamanhoPopulacao.Text = "Quantidade de Gerações: " + pop.QtdeGeracoes;
            lblTamanhoGeracao.Text = "Tamanho da Geração: " + pop.TamanhoGeracao;
            lblTamanhoAlelo.Text = "Tamanho Alelo (possibilidade dos Genes): " + pop.qtdeCidades;

            Individuo melhorIndividuo = new Individuo();
            string geracaoMelhorIndividuo = "";

            foreach (Geracao geracao in pop.Geracoes)
            {
                foreach (Individuo individuo in geracao.Individuos)
                {
                    if (melhorIndividuo.Fitness <= 0 || individuo.Fitness < melhorIndividuo.Fitness)
                    {
                        melhorIndividuo = individuo;
                        geracaoMelhorIndividuo = geracao.Codigo;
                    }
                }
            }

            lblMelhor.Text = "O Menor Fitness é do Indivíduo " + melhorIndividuo.Codigo + " da Geração " + geracaoMelhorIndividuo + " com o Fitness = " + melhorIndividuo.Fitness + " -> Seu Cromossomo é " + GetCromossomosParaVisualizacao(melhorIndividuo.Cromossomos);
        }

        public string GetCromossomosParaVisualizacao(List<int> cromossomo)
        {
            string retorno = "";
            int alelo = cromossomo.Count;

            if (cromossomo.Count > 0)
            {
                foreach (int gene in cromossomo)
                {
                    retorno += new Alfabeto(alelo).GetLetraByPosicao(gene) + ", ";
                }

                if (this.UltimoGene)
                    retorno += new Alfabeto(alelo).GetLetraByPosicao(cromossomo.First());
            }

            return retorno;
        }
    }
}
